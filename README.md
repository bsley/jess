Website portfolio, storefront, and blog for artist and painter Jessica Jorgensen - jessicajorgensenart.com

### Tools and technology ###

* Grunt
* Stylus
* MAMP (for local environment)
* CraftCMS