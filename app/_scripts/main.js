$(document).ready(function() {

  /* LINKING ENTIRE DIVS */

  $(".linked").click(function(){
    window.location=$(this).find("a").attr("href"); 
    console.log('clicked');
    return false;
  });

  // MENU FUNCTIONALITY

  var menuHit = $(".menuHit");
  var nav = $("nav.miniMenu");
  var navItems = $("nav li");
  var body = $("body");
  var logo = $(".logo");
  var menuStatus = false;
  
  menuHit.hammer().bind("tap", function() {
    
    if (!menuStatus) {
      menuStatus = true;
      console.log("tapped and menu is now on");
      $(this).find(".menuBtn div").addClass('open');
      nav.removeClass('hidden visuallyhidden');
      body.addClass('locked');
      // logo.addClass('hidden');
      navItems.velocity("transition.fadeIn", { 
		  	duration: 200,
		  	stagger: 20,
		});
     event.preventDefault();

    } else {
      menuStatus = false;
      console.log("tapped and menu is now off")
      $(this).find(".menuBtn div").removeClass('open');
      nav.addClass('hidden visuallyhidden');
      body.removeClass('locked');
      // logo.removeClass('hidden');
      navItems.hide();
    }
    
  });
});






  